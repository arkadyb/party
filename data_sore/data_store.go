package data_sore

import "github.com/party/types"

/*
	Any type that implements data store Interface may be used as customers storage
	in PartyManager
 */
type Interface interface{
	GetCustomers() ([]types.Customer, error)
}