package data_sore

import (
	"github.com/party/types"
	"io/ioutil"
	"encoding/json"
	"os"
	"strings"
	"errors"
)

//NewFileStore creates a new customers json store
func NewFileStore(filename string) (*FileStore, error){
	if strings.TrimSpace(filename)==""{
		return nil, errors.New("filename cant be empty")
	}

	fs:=new(FileStore)
	fs.filename = strings.TrimSpace(filename)

	return fs, nil
}

//FileStore implements data store Interface for customers list saved in json file
type FileStore struct{
	filename string
}

//GetCustomers returns a list of customers from given json file; or error
func (fs *FileStore) GetCustomers() ([]types.Customer, error){
	if _, err := os.Stat(fs.filename); err != nil {
		return nil, err
	}

	bts, err := ioutil.ReadFile(fs.filename)
	if err != nil {
		return nil, err
	}

	customers := []types.Customer{}
	err = json.Unmarshal(bts, &customers)
	if err != nil {
		return nil, err
	}

	return customers, nil
}
