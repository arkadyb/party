package data_sore

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/party/types"
)

func TestNewFileStore(t *testing.T) {
	t.Run("Success", func(t *testing.T){
		fs, err := NewFileStore("fake.json")
		assert.NoError(t, err)
		if assert.NotNil(t, fs){
			assert.Equal(t, "fake.json", fs.filename)
		}
	})

	t.Run("Empty filename; error", func(t *testing.T){
		fs, err := NewFileStore("")
		assert.Error(t, err)
		assert.Nil(t, fs)
	})
}

func TestFileStore_GetCustomers(t *testing.T) {
	t.Run("File not exists", func(t *testing.T){
		fs, _ := NewFileStore("fake_file.json")
		customers, err := fs.GetCustomers()
		assert.Error(t, err)
		assert.Nil(t, customers)
	})

	t.Run("Malformed json", func(t *testing.T){
		fs, _ := NewFileStore("test_data/customers_bad.json")
		customers, err := fs.GetCustomers()
		assert.Error(t, err)
		assert.Nil(t, customers)
	})

	t.Run("Success", func(t *testing.T){
		fs, _ := NewFileStore("test_data/customers.json")
		customers, err := fs.GetCustomers()
		assert.NoError(t, err)
		if assert.NotNil(t, customers){
			assert.Len(t, customers,2)

			assert.Equal(t, types.Customer{
				ID:12,
				Name: "Christina McArdle",
				Latitude:52.986375,
				Longitude:-6.043701,
			}, customers[0])

			assert.Equal(t, types.Customer{
				ID:1,
				Name: "Alice Cahill",
				Latitude:51.92893,
				Longitude:-10.27699,
			}, customers[1])
		}
	})
}