package main

import (
	"fmt"

	"github.com/party/manager"
	"github.com/party/data_sore"
)

func main() {
	fileStore, err:=data_sore.NewFileStore("customers.json")
	if err!=nil{
		fmt.Println(err.Error())
		return
	}

	mgr, _ := manager.NewPartyManager(fileStore)
	customersToBeInvited,_ := mgr.GetCustomersForParty(53.3393, -6.2576841, 100, manager.Asc)

	for _, c := range customersToBeInvited {
		fmt.Printf("%d %s\n", c.ID, c.Name)
	}
	fmt.Println("---------------------")
	fmt.Printf("Total: %d customers to invite", len(customersToBeInvited))
}
