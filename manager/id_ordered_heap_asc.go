package manager

import "github.com/party/types"

/*
	idOrderedHeapAsc implements golang`s heap interface to store and read customers ordered by ID ascendingly
	More about heap: https://golang.org/pkg/container/heap/
 */

type idOrderedHeapAsc []types.Customer

func (h idOrderedHeapAsc) Len() int           { return len(h) }
func (h idOrderedHeapAsc) Less(i, j int) bool { return h[i].ID < h[j].ID }
func (h idOrderedHeapAsc) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *idOrderedHeapAsc) Push(x interface{}) {
	*h = append(*h, x.(types.Customer))
}

func (h *idOrderedHeapAsc) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}