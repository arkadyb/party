package manager

import (
	"testing"
	"container/heap"
	"github.com/party/types"
	"github.com/stretchr/testify/assert"
)

func TestIdOrderedHeapAsc(t *testing.T){
	customers := new(idOrderedHeapAsc)
	heap.Init(customers)
	for _, i := range []int{5,7,1,9}{
		heap.Push(customers, types.Customer{ID:i})
	}

	var ids []int
	for customers.Len()>0 {
		ids=append(ids, heap.Pop(customers).(types.Customer).ID)
	}

	assert.Equal(t, []int{1,5,7,9}, ids)
}