package manager

import "github.com/party/types"

/*
	idOrderedHeapAsc implements golang`s heap interface to store and read customers ordered by ID descendingly
	More about heap: https://golang.org/pkg/container/heap/
 */

type idOrderedHeapDesc []types.Customer

func (h idOrderedHeapDesc) Len() int           { return len(h) }
func (h idOrderedHeapDesc) Less(i, j int) bool { return h[i].ID > h[j].ID }
func (h idOrderedHeapDesc) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *idOrderedHeapDesc) Push(x interface{}) {
	*h = append(*h, x.(types.Customer))
}

func (h *idOrderedHeapDesc) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}