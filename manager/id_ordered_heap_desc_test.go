package manager

import (
	"testing"
	"container/heap"
	"github.com/stretchr/testify/assert"
	"github.com/party/types"
)

func TestIdOrderedHeapDesc(t *testing.T){
	customers := new(idOrderedHeapDesc)
	heap.Init(customers)
	for _, i := range []int{5,7,1,9}{
		heap.Push(customers, types.Customer{ID:i})
	}

	var ids []int
	for customers.Len()>0 {
		ids=append(ids, heap.Pop(customers).(types.Customer).ID)
	}

	assert.Equal(t, []int{9,7,5,1}, ids)
}
