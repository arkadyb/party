package manager

import (
	"container/heap"
	"fmt"
)

//Order values used in PartyManager to return customers collection in desired order
type Order int
const (
	Asc Order = 1
	Desc Order = 2
)

//heapFactory a factory implementation for defined heaps implementations
type heapFactory struct{
	implementations map[Order]heap.Interface
}

//getHeap returns concrete heap implementation; or error
func (hf *heapFactory) getHeap(order Order) (heap.Interface, error){
	if f,ok:=hf.implementations[order];ok{
		return f, nil
	}

	return nil, fmt.Errorf("Could not find ordered heap implemetation for Order value %d", order)
}

//newHeapFactory creates a new heapFactory
func newHeapFactory() *heapFactory{
	hf:=new(heapFactory)
	hf.implementations = make(map[Order]heap.Interface)
	hf.implementations[Asc]=new(idOrderedHeapAsc)
	hf.implementations[Desc]=new(idOrderedHeapDesc)

	return hf
}