package manager

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"container/heap"
)

func TestNewHeapFactory(t *testing.T){
	hf:=newHeapFactory()
	assert.NotNil(t, hf)
	assert.NotNil(t, hf.implementations)
	assert.Len(t, hf.implementations, 2)
}

func TestGetHeap(t *testing.T){
	t.Run("Asc heap created", func(t *testing.T){
		hf:=newHeapFactory()

		ascHeap, err := hf.getHeap(Asc)
		assert.NoError(t, err)
		if assert.NotNil(t, ascHeap){
			assert.Implements(t, (*heap.Interface)(nil), ascHeap)
		}
	})

	t.Run("Desc heap created", func(t *testing.T){
		hf:=newHeapFactory()

		ascHeap, err := hf.getHeap(Desc)
		assert.NoError(t, err)
		if assert.NotNil(t, ascHeap){
			assert.Implements(t, (*heap.Interface)(nil), ascHeap)
		}
	})

	t.Run("No implementation found", func(t *testing.T){
		hf:=newHeapFactory()

		ascHeap, err := hf.getHeap(-1)
		assert.Error(t, err)
		assert.Nil(t, ascHeap)
	})
}