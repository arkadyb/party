package manager

import (
	"github.com/party/types"
	"github.com/party/utils"
	"github.com/party/data_sore"
	"container/heap"
	"errors"
)

//NewPartyManager creates a PartyManager object
func NewPartyManager(store data_sore.Interface) (*PartyManager, error) {
	if store == nil{
		return nil, errors.New("store cant be nil")
	}

	mgr := new(PartyManager)
	mgr.dataStore = store
	mgr.hFactory = newHeapFactory()

	return mgr, nil
}

//PartyManager used to manipulate customer`s collections loaded with given DataStore implementation
type PartyManager struct {
	dataStore data_sore.Interface
	hFactory *heapFactory
}

//GetCustomersForParty returns a list of customers within given distance from meet point specified with latitude and longitude values; or error
func (mgr *PartyManager) GetCustomersForParty(latitude, longitude float64, distance int, order Order) ([]types.Customer, error) {

	//Load customers with given strategy
	customers, err := mgr.dataStore.GetCustomers()
	if err!=nil {
		return nil, err
	}

	if len(customers) == 0 {
		return []types.Customer{}, nil
	}

	//Loads proper heap implementation for required customers ordering
	customersHeap, err:=mgr.hFactory.getHeap(order)
	if err!=nil{
		return nil, err
	}
	heap.Init(customersHeap)

	//Iterate over loaded customers
	for _, customer := range customers {
		if utils.GetDistance(customer.Latitude, customer.Longitude, latitude, longitude) < float64(distance) {
			heap.Push(customersHeap, customer)
		}
	}

	if customersHeap.Len() > 0 {
		results := []types.Customer{}
		for customersHeap.Len() > 0 {
			//Collect values by poping items from heap
			results = append(results, heap.Pop(customersHeap).(types.Customer))
		}

		return results, nil
	}

	return []types.Customer{}, nil
}
