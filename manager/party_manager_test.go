package manager

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"github.com/party/data_sore"
)

func TestPartyManager_GetCustomersForParty(t *testing.T) {
	t.Run("Bad data source", func (t *testing.T){
		fs, _:=data_sore.NewFileStore("fake.json")
		manager, _ := NewPartyManager(fs)
		customers, err := manager.GetCustomersForParty(0,0,100, Asc)
		assert.Error(t, err)
		assert.Nil(t, customers)
	})

	t.Run("Empty customers collection", func (t *testing.T){
		fs, _:=data_sore.NewFileStore("test_data/customers_empty.json")
		manager, _ := NewPartyManager(fs)
		customers, err := manager.GetCustomersForParty(0,0,100, Asc)
		assert.NoError(t, err)
		if assert.NotNil(t, customers){
			assert.Len(t, customers, 0)
		}
	})

	t.Run("No heap", func (t *testing.T){
		fs, _:=data_sore.NewFileStore("test_data/customers.json")
		manager, _ := NewPartyManager(fs)
		customers, err := manager.GetCustomersForParty(0,0,100, 3)
		assert.Error(t, err)
		assert.Nil(t, customers)
	})

	t.Run("Success", func (t *testing.T){
		fs, _:=data_sore.NewFileStore("test_data/customers.json")
		manager, _ := NewPartyManager(fs)
		customers, err := manager.GetCustomersForParty(53.3393, -6.2576841, 100, Asc)
		assert.NoError(t, err)
		if assert.NotNil(t, customers){
			assert.Len(t, customers, 1)
		}
	})

}

func TestNewPartyManager(t *testing.T) {

	t.Run("Nil data store", func(t *testing.T){
		manager, err := NewPartyManager(nil)
		assert.Error(t, err)
		assert.Nil(t, manager)
	})

	t.Run("Success", func(t *testing.T){
		fs, _:=data_sore.NewFileStore("fake.json")
		manager, err := NewPartyManager(fs)
		assert.NoError(t, err)
		if assert.NotNil(t, manager){
			if assert.NotNil(t, manager.hFactory){
				assert.Equal(t, fs, manager.dataStore)
			}
		}
	})
}
