package types

//Customer object contains a set of fields to read a customers data_sore from task`s described json file ('customers.json')
type Customer struct {
	ID        int     `json:"user_id"`
	Name      string  `json:""`
	Latitude  float64 `json:",string"`
	Longitude float64 `json:",string"`
}
