package utils

import "math"

//GetDistance returns a distance between two given points passed as latitude/longitude values
//Returns a distance in kilometers
func GetDistance(latitude1, longitude1, latitude2, longitude2 float64) float64 {
	var r float64
	r = 6371 //Earth radius in km

	dlat := toRadians(latitude2 - latitude1)
	dlon := toRadians(longitude2 - longitude1)

	a := math.Pow(math.Sin(dlat/2), 2) + math.Cos(toRadians(latitude1))*math.Cos(toRadians(latitude2))*math.Pow(math.Sin(dlon/2), 2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	return r * c
}

func toRadians(degrees float64) float64 {
	return degrees * (math.Pi / 180)
}
