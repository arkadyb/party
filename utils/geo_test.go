package utils

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestGetDistance(t *testing.T) {
	t.Run("Same Point", func(t *testing.T){
		distance:=GetDistance(1,1,1,1)
		assert.Equal(t, float64(0), distance)
	})

	t.Run("Distance to PTC office", func(t *testing.T){
		distance:=GetDistance(51.92893, -10.27699, 53.251382, -6.183314)
		assert.Equal(t, 313.137619817433, distance)
	})
}

func TestToRadians(t *testing.T){
	t.Run("0 degrees", func(t *testing.T){
		radiands := toRadians(0)
		assert.Zero(t, radiands)
	})

	t.Run("180 degrees", func(t *testing.T){
		radians := toRadians(180)
		assert.Equal(t, 3.141592653589793, radians)
	})
}
